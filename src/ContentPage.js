import React, { useState } from 'react';
import { Button, Space, DatePicker, Select, notification, Typography } from 'antd';
import styled from 'styled-components'

const { Title, Text } = Typography;
const { Option } = Select;

// function onChange(date, dateString) {
//     console.log(date, dateString);
// }

// function handleChange(value) {
//     console.log(`selected ${value}`);
// }

const Div = styled.div`
    width: 100%;
    display: flex;
    flex-direction: flex-row;
    justify-content: center;
`;

const openNotification = () => {
    notification.open({
      message: 'Prediction',
      description:
        'We will train our neural networks for your task and will send you result.',
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  };

const ContentPage = () => {
    const [visibility, setVisibility] = useState(false);
    const [selected, setSelected] = useState("");
    const [selectedDate, setSelectedDate] = useState();
    
    console.log(selectedDate)
    return (
        <div>
            <Div>
                <Space>
                    {/* <Input placeholder="Basic usage" size="middle" /> */}
                    <Select defaultValue={selected} style={{ width: 120 }} onChange={value => setSelected(value)}>
                        <Option value="jack">Gold</Option>
                        <Option value="lucy">Silver</Option>
                        <Option value="Yiminghe">Diamonds</Option>
                    </Select>
                    <DatePicker showTime onChange={(date, dateString) => setSelectedDate(dateString)} />
                    <Button type="primary" shape="round" size='large' onClick={() => {if (!visibility) openNotification(); setVisibility(!visibility);}}>Predict</Button>
                </Space>
            </Div>
            <div style={visibility ? {} : {"display": "none"}}>
                <Title level={4}>Result</Title>
                <Text>You entered {selected}, date {selectedDate}</Text>
            </div>
        </div>
    );
};

export default ContentPage;
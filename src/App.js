import React from 'react';
import ContentPage from './ContentPage';
import { Layout, Menu, Breadcrumb } from 'antd';
import 'antd/dist/antd.css';
import { Typography } from 'antd';
import styled from 'styled-components'

const { Title } = Typography;
const { Header, Footer, Content } = Layout;

const SiteLayoutContent = styled.div`
  background: #fff;
  padding: 24px;
  min-height: 280px;
  height: 100%;
`;

function App() {
  return (
    <Layout className="layout" style={{height:"100vh"}}>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
            <Menu.Item key="1">Home</Menu.Item>
            <Menu.Item key="2">Prediction</Menu.Item>
            <Menu.Item key="3">Contact</Menu.Item>
          </Menu>
        </Header>
        <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>Prediction</Breadcrumb.Item>
        </Breadcrumb>
        <SiteLayoutContent>
          <Title level={3}>Market prediction</Title>
          <ContentPage />
        </SiteLayoutContent>
      </Content>
      <Footer style={{ textAlign: 'center' }}>©2020 Jan Čuhel, Jan Pfeiffer</Footer>
    </Layout>
  );
}

export default App;
